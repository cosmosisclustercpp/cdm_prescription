/* This code, originall written by Daniel Eisenstein and Wayne Hu, has been
 * into a CosmoSIS Module by:
 *
 *          Jackson O'Donnell
 *    (jacksonhodonnell@gmail.com)
 *
 * During July-August 2018, at Fermilab, as part of the Dark Energy Survey's
 * Year 3 Cluster Cosmology program.
 */

/* Fitting Formulae for CDM + Baryon + Massive Neutrino (MDM) cosmologies. */
/* Daniel J. Eisenstein & Wayne Hu, Institute for Advanced Study */

/* There are two primary routines here, one to set the cosmology, the
other to construct the transfer function for a single wavenumber k. 
You should call the former once (per cosmology) and the latter as 
many times as you want. */

/* TFmdm_set_cosm() -- User passes all the cosmological parameters as
 *  arguments; the routine sets up all of the scalar quantites needed 
 *  computation of the fitting formula.  The input parameters are: 
 *  1) omega_matter -- Density of CDM, baryons, and massive neutrinos,
 *              in units of the critical density. 
 *  2) omega_baryon -- Density of baryons, in units of critical. 
 *  3) omega_hdm    -- Density of massive neutrinos, in units of critical 
 *  4) degen_hdm    -- (Int) Number of degenerate massive neutrino species 
 *  5) omega_lambda -- Cosmological constant 
 *  6) hubble       -- Hubble constant, in units of 100 km/s/Mpc 
 *  7) redshift     -- The redshift at which to evaluate */

/* TFmdm_onek_mpc() -- User passes a single wavenumber, in units of Mpc^-1.
 *  Routine returns the transfer function from the Eisenstein & Hu
 *  fitting formula, based on the cosmology currently held in the 
 *  internal variables.  The routine returns T_cb (the CDM+Baryon
 *  density-weighted transfer function), although T_cbn (the CDM+
 *  Baryon+Neutrino density-weighted transfer function) is stored
 *  in the global variable tf_cbnu. */

/* We also supply TFmdm_onek_hmpc(), which is identical to the previous
 *  routine, but takes the wavenumber in units of h Mpc^-1. */

/* We hold the internal scalar quantities in global variables, so that
 * the user may access them in an external program, via "extern" declarations. */

/* Please note that all internal length scales are in Mpc, not h^-1 Mpc! */

#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "transfer_function.h"

#define SQR(a) ((a)*(a))

/* By default, these functions return tf_cb */

/* ------------------------- TFmdm_set_cosm() ------------------------ */
int
TFmdm_set_cosm(struct transfer_function_cosmology *cosm,
               double omega_matter, double omega_baryon, double omega_hdm,
               int degen_hdm, double omega_lambda, double hubble, double redshift)
/* This routine takes cosmological parameters and a redshift and sets up
all the internal scalar quantities needed to compute the transfer function. */
/* INPUT: omega_matter -- Density of CDM, baryons, and massive neutrinos,
 *              in units of the critical density. *
 *    omega_baryon -- Density of baryons, in units of critical.
 *    omega_hdm    -- Density of massive neutrinos, in units of critical
 *    degen_hdm    -- (Int) Number of degenerate massive neutrino species
 *    omega_lambda -- Cosmological constant
 *    hubble       -- Hubble constant, in units of 100 km/s/Mpc
 *    redshift     -- The redshift at which to evaluate
 * OUTPUT: Returns 0 if all is well, 1 if a warning was issued.  Otherwise,
 *  sets `cosm` for use in TFmdm_onek_mpc[h]() */
{
    assert(cosm != NULL);
    double z_drag_b1, z_drag_b2, omega_denom;
    int qwarn = 0;

    cosm->theta_cmb = 2.728/2.7;  /* Assuming T_cmb = 2.728 K */

    /* Look for strange input */
    if (omega_baryon<0.0) {
        fprintf(stderr,
          "TFmdm_set_cosm(): Negative omega_baryon set to trace amount.\n");
        qwarn = 1;
    }

    if (omega_hdm<0.0) {
        fprintf(stderr,
          "TFmdm_set_cosm(): Negative omega_hdm set to trace amount.\n");
        qwarn = 1;
    }

    if (hubble<=0.0) {
        fprintf(stderr,"TFmdm_set_cosm(): Negative Hubble constant illegal.\n");
        assert(false);
    } else if (hubble>2.0) {
        fprintf(stderr,"TFmdm_set_cosm(): Hubble constant should be in units of 100 km/s/Mpc.\n");
        qwarn = 1;
    }

    if (redshift<=-1.0) {
        fprintf(stderr,"TFmdm_set_cosm(): Redshift < -1 is illegal.\n");
        exit(1);
    } else if (redshift>99.0) {
        fprintf(stderr,
          "TFmdm_set_cosm(): Large redshift entered.  TF may be inaccurate.\n");
        qwarn = 1;
    }

    if (degen_hdm<1)
        degen_hdm = 1;
    cosm->num_degen_hdm = (double) degen_hdm; 
    /* Have to save this for TFmdm_onek_mpc() */
    /* This routine would crash if baryons or neutrinos were zero, 
    so don't allow that */
    if (omega_baryon <= 0)
        omega_baryon = 1e-5;
    if (omega_hdm <= 0)
        omega_hdm = 1e-5;

    cosm->omega_curv = 1.0 - omega_matter-omega_lambda;
    cosm->omhh = omega_matter * SQR(hubble);
    cosm->obhh = omega_baryon * SQR(hubble);
    cosm->onhh = omega_hdm * SQR(hubble);
    cosm->f_baryon = omega_baryon / omega_matter;
    cosm->f_hdm = omega_hdm / omega_matter;
    cosm->f_cdm = 1.0 - cosm->f_baryon - cosm->f_hdm;
    cosm->f_cb = cosm->f_cdm + cosm->f_baryon;
    cosm->f_bnu = cosm->f_baryon + cosm->f_hdm;

    /* Compute the equality scale. */
    cosm->z_equality = 25000.0 * cosm->omhh / SQR(SQR(cosm->theta_cmb));  /* Actually 1+z_eq */
    cosm->k_equality = 0.0746 * cosm->omhh / SQR(cosm->theta_cmb);

    /* Compute the drag epoch and sound horizon */
    z_drag_b1 = 0.313 * pow(cosm->omhh, -0.419) * (1 + 0.607 * pow(cosm->omhh, 0.674));
    z_drag_b2 = 0.238 * pow(cosm->omhh, 0.223);
    cosm->z_drag = 1291 * pow(cosm->omhh , 0.251) / (1.0 + 0.659 * pow(cosm->omhh , 0.828)) *
                   (1.0 + z_drag_b1 * pow(cosm->obhh, z_drag_b2));
    cosm->y_drag = cosm->z_equality / (1.0 + cosm->z_drag);
    cosm->sound_horizon_fit = 44.5*log(9.83/cosm->omhh)/sqrt(1.0+10.0*pow(cosm->obhh,0.75));

    /* Set up for the free-streaming & infall growth function */
    cosm->p_c = 0.25*(5.0-sqrt(1+24.0*cosm->f_cdm));
    cosm->p_cb = 0.25*(5.0-sqrt(1+24.0*cosm->f_cb));

    omega_denom = omega_lambda+SQR(1.0+redshift)*(cosm->omega_curv+
            omega_matter*(1.0+redshift));
    cosm->omega_lambda_z = omega_lambda/omega_denom;
    cosm->omega_matter_z = omega_matter*SQR(1.0+redshift)*(1.0+redshift)/omega_denom;
    cosm->growth_k0 = cosm->z_equality / (1.0+redshift) * 2.5 * cosm->omega_matter_z /
                      (pow(cosm->omega_matter_z,4.0/7.0) - cosm->omega_lambda_z +
                        (1.0 + cosm->omega_matter_z/2.0) * (1.0 + cosm->omega_lambda_z / 70.0));
    cosm->growth_to_z0 = cosm->z_equality*2.5*omega_matter/(pow(omega_matter,4.0/7.0) -
                   omega_lambda + (1.0+omega_matter/2.0)*(1.0+omega_lambda/70.0));
    cosm->growth_to_z0 = cosm->growth_k0/cosm->growth_to_z0;  
    
    /* Compute small-scale suppression */
    cosm->alpha_nu = cosm->f_cdm/cosm->f_cb*(5.0-2.*(cosm->p_c+cosm->p_cb))/(5.-4.*cosm->p_cb)*
                     pow(1 + cosm->y_drag, cosm->p_cb - cosm->p_c) *
                     (1 + cosm->f_bnu * (-0.553 + 0.126 * cosm->f_bnu * cosm->f_bnu))/
                     (1 - 0.193*sqrt(cosm->f_hdm * cosm->num_degen_hdm) + 0.169 * cosm->f_hdm * pow(cosm->num_degen_hdm, 0.2))*
                     (1 + (cosm->p_c - cosm->p_cb) / 2 * (1 + 1 / (3. - 4. * cosm->p_c) / (7. - 4. * cosm->p_cb)) / (1 + cosm->y_drag));
    cosm->alpha_gamma = sqrt(cosm->alpha_nu);
    cosm->beta_c = 1/(1-0.949*cosm->f_bnu);

    /* Done setting scalar variables */
    cosm->hhubble = hubble;   /* Need to pass Hubble constant to TFmdm_onek_hmpc() */
    return qwarn;
}

/* ---------------------------- TFmdm_mpc() ---------------------- */

void
TFmdm_mpc(const struct transfer_function_cosmology *cosm,
          size_t nks,
          const double *kk,
          double *tf_cb, double *tf_cbnu,
          double *growth_cb, double *growth_cbnu)
/* Given a wavenumber in Mpc^-1, return the transfer function for the
cosmology held in the global variables. */
/* Input: kk -- Wavenumber in Mpc^-1 */
/* Output: The following are set as global variables:
    growth_cb -- the transfer function for density-weighted
            CDM + Baryon perturbations. 
    growth_cbnu -- the transfer function for density-weighted
            CDM + Baryon + Massive Neutrino perturbations. */
/* The function returns growth_cb */
{
    assert(cosm != NULL);

    for (size_t i = 0; i < nks; i++) {
        // Wavenumber rescaled by \Gamma
        const double qq = kk[i] / cosm->omhh * SQR(cosm->theta_cmb);

        /* Compute the scale-dependent growth functions */
        // y_freestream is the Epoch of free-streaming for a given scale
        const double y_freestream = 17.2 * cosm->f_hdm * (1 + 0.488 * pow(cosm->f_hdm, -7.0/6.0))*
                       SQR(cosm->num_degen_hdm * qq/cosm->f_hdm),
                     temp1 = pow(cosm->growth_k0, 1.0-cosm->p_cb),
                     temp2 = pow(cosm->growth_k0 / (1 + y_freestream), 0.7);

        const double growth_cb_local = pow(1.0+temp2, cosm->p_cb/0.7)*temp1,
                     growth_cbnu_local = pow(pow(cosm->f_cb,0.7/cosm->p_cb)+temp2, cosm->p_cb/0.7)*temp1;

        if (growth_cb)
            growth_cb[i] = growth_cb_local;
        if (growth_cbnu)
            growth_cbnu[i] = growth_cbnu_local;

        if (!(tf_cb || tf_cbnu))
            continue;

        /* Compute the master function */
        // Effective \Gamma & Wavenumber Rescaled by Effective \Gamma
        const double gamma_eff = cosm->omhh*(cosm->alpha_gamma+(1-cosm->alpha_gamma)/
                           (1+SQR(SQR(kk[i]*cosm->sound_horizon_fit*0.43)))),
                     qq_eff = qq*cosm->omhh/gamma_eff;

        // Suppressed TF
        const double tf_sup_L = log(2.71828+1.84*cosm->beta_c*cosm->alpha_gamma*qq_eff),
                     tf_sup_C = 14.4+325/(1+60.5*pow(qq_eff,1.11)),
                     tf_sup = tf_sup_L/(tf_sup_L+tf_sup_C*SQR(qq_eff));

        // Wavenumber rescaled by maximal free streaming
        const double qq_nu = 3.92 * qq * sqrt(cosm->num_degen_hdm / cosm->f_hdm),
                     // Correction near maximal free streaming
                     max_fs_correction = 1+1.2*pow(cosm->f_hdm,0.64)*pow(cosm->num_degen_hdm,0.3+0.6*cosm->f_hdm)/
                                         (pow(qq_nu,-1.6)+pow(qq_nu,0.8)),
                     // Master TF
                     tf_master = tf_sup * max_fs_correction;

        /* Now compute the CDM+HDM+baryon transfer functions */
        if (tf_cb)
            tf_cb[i] = tf_master * growth_cb_local / cosm->growth_k0;
        if (tf_cbnu)
            tf_cbnu[i] = tf_master * growth_cbnu_local / cosm->growth_k0;
    }
}

/* ---------------------------- TFmdm_hmpc() ---------------------- */

void
TFmdm_hmpc(const struct transfer_function_cosmology *cosm,
           size_t nks,
           const double *kk,
           double *tf_cb, double *tf_cbnu,
           double *growth_cb, double *growth_cbnu)
/* Given a wavenumber in h Mpc^-1, return the transfer function for the
cosmology held in the global variables. */
/* Input: kk -- Wavenumber in h Mpc^-1 */
/* Output: The following are set as global variables:
    growth_cb -- the transfer function for density-weighted
            CDM + Baryon perturbations. 
    growth_cbnu -- the transfer function for density-weighted
            CDM + Baryon + Massive Neutrino perturbations. */
/* The function returns growth_cb */
{
    double hkk[nks];
    for (size_t i = 0; i < nks; i++)
        hkk[i] = kk[i] * cosm->hhubble;
    TFmdm_mpc(cosm, nks, hkk,
              tf_cb, tf_cbnu,
              growth_cb, growth_cbnu);
}
