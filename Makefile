CC=gcc
CFLAGS=-Wall -O3 -std=c11 -mtune=native
COSMOSIS_DIR=/cosmosis

libcdm_prescription.so: module.c transfer_function.o transfer_function.h
	$(CC) $(CFLAGS) -fPIC -I$(COSMOSIS_DIR) -shared -o $@ module.c transfer_function.c -L$(COSMOSIS_DIR)/cosmosis/datablock/ -lcosmosis

transfer_function.o: transfer_function.c transfer_function.h
