#include <stdlib.h>
#include <cosmosis/datablock/c_datablock.h>
#include "transfer_function.h"

/* There appears to be a `c_datablock_replace_double_array` in `libcosmosis`,
 * but it is not defined in c_datablock.h, so we define it here.
 */
DATABLOCK_STATUS
c_datablock_replace_double_array(c_datablock *s,
                                 const char *section,
                                 const char *name,
                                 double const *val,
                                 int ndims,
                                 int const *extents);

void *
setup(c_datablock *db)
{
    return NULL;
}

DATABLOCK_STATUS
execute(c_datablock *blk, void *config)
{
    double omega_baryon, omega_hdm, omega_cdm,
           omega_lambda, hubble;
    // Number of massive neutrinos - Assumed degenerate
    int degen_hdm;
    int retval = 0;

    /* Read all of the single-value parameters we need */
    if ((retval = c_datablock_get_double(blk, "cosmological_parameters", "omega_b", &omega_baryon))
            != DBS_SUCCESS)
        return retval;
    if ((retval = c_datablock_get_double(blk, "cosmological_parameters", "omega_nu", &omega_hdm))
            != DBS_SUCCESS)
        return retval;
    if ((retval = c_datablock_get_double(blk, "cosmological_parameters", "omega_c", &omega_cdm))
            != DBS_SUCCESS)
        return retval;
    if ((retval = c_datablock_get_double(blk, "cosmological_parameters", "omega_lambda", &omega_lambda))
            != DBS_SUCCESS)
        return retval;
    if ((retval = c_datablock_get_double(blk, "cosmological_parameters", "hubble", &hubble))
            != DBS_SUCCESS)
        return retval;
    if ((retval = c_datablock_get_int(blk, "cosmological_parameters", "massive_nu", &degen_hdm))
            != DBS_SUCCESS)
        return retval;

    /* Read the array parameters */
    int nzs, nks, p_k_extents[2];
    double *zs = NULL,
           *k_h = NULL,
           *p_k_raw = NULL;

    if ((retval = c_datablock_get_double_array_1d(blk, "matter_power_lin", "z", &zs, &nzs))
            != DBS_SUCCESS)
        return retval;

    if ((retval = c_datablock_get_double_array_1d(blk, "matter_power_lin", "k_h", &k_h, &nks))
            != DBS_SUCCESS)
        return retval;

    p_k_extents[0] = nzs;
    p_k_extents[1] = nks;
    p_k_raw = malloc(sizeof(double) * nzs * nks);

    if ((retval = c_datablock_get_double_array(blk, "matter_power_lin", "p_k", p_k_raw, 2, p_k_extents))
            != DBS_SUCCESS)
        return retval;

    if ((retval = c_datablock_put_double_array(blk, "matter_power_lin", "p_k_orig", p_k_raw, 2, p_k_extents))
            != DBS_SUCCESS)
        return retval;

    double (*p_k)[nzs][nks] = (double (*)[nzs][nks]) p_k_raw;
    double omega_m = omega_baryon + omega_hdm + omega_cdm;
    struct transfer_function_cosmology cosm;
    for (size_t i = 0; i < nzs; i++) {
        TFmdm_set_cosm(&cosm, omega_m, omega_baryon, omega_hdm,
                       degen_hdm, omega_lambda, hubble / 100.0, zs[i]);

        /* The conversion is:
         *
         *      P_{cdm}(k, z) = [(omega_{cdm} T_{cdm}(k, z) + omega_b T_b(k, z)) / (omega_{cdm} + omega_b)]^2 P_m(k, z)
         *
         * We will approximate it as: 
         *
         *      P_{cdm}(k, z) = (T_{cdm + b}(k, z) / T_m(k, z))^2 P_m(k, z)
         *
         * Because Eisenstein & Hu's code does not separate T_{cdm} and T_b.
         */
        double tf_cb[nks],
               tf_m[nks];
        TFmdm_hmpc(&cosm, nks, k_h,
                   tf_cb, tf_m,
                   NULL, NULL);

        // Apply P_{cdm} transformation
        for (size_t j = 0; j < nks; j++) {
            const double ratio = tf_cb[j] / tf_m[j];
            (*p_k)[i][j] *= ratio * ratio;
        }
    }

    if ((retval = c_datablock_replace_double_array(blk, "matter_power_lin", "p_k",
                                                   p_k_raw, 2, p_k_extents))
            != DBS_SUCCESS)
        return retval;

    free(p_k_raw);

    return DBS_SUCCESS;
}

int
cleanup(void *config)
{
    return 0;
}
