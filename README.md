# Cold Dark Matter Prescription for Power Function

This module applies the 'Cold Dark Matter Prescription' given by [Costanzi et al.](https://arxiv.org/pdf/1311.1514.pdf) to the output of CAMB, according to Equation (4.4):

```
P_{cdm}(k, z) = P_m(k, z)[(\Omega_b T_b(k, z) + \Omega_{cdm} T_{cdm}) / (\Omega_b + \Omega_{cdm}) / T_m(k, z)]^2
```

Using the Transfer Functions from [Wayne Hu](http://background.uchicago.edu/~whu/transfer/transferpage.html). Currently, Hu's implementation does not compute `T_{cdm}` and `T_b` separately, but only `T_{cdm + b}`, so we approximate the above equation by:

```
P_{cdm}(k, z) = P_m(k, z) (T_{cdm + b} / T_m(k, z))^2
```
