#ifndef _COSMOMOD_TRANSFER_FUNCTION_H
#define _COSMOMOD_TRANSFER_FUNCTION_H
/* This code, originall written by Daniel Eisenstein and Wayne Hu, has been
 * into a CosmoSIS Module by:
 *
 *          Jackson O'Donnell
 *    (jacksonhodonnell@gmail.com)
 *
 * During July-August 2018, at Fermilab, as part of the Dark Energy Survey's
 * Year 3 Cluster Cosmology program.
 */

/* Fitting Formulae for CDM + Baryon + Massive Neutrino (MDM) cosmologies. */
/* Daniel J. Eisenstein & Wayne Hu, Institute for Advanced Study */

/* There are two primary routines here, one to set the cosmology, the
other to construct the transfer function for a single wavenumber k. 
You should call the former once (per cosmology) and the latter as 
many times as you want. */

/* TFmdm_set_cosm() -- User passes all the cosmological parameters as
    arguments; the routine sets up all of the scalar quantites needed 
    computation of the fitting formula.  The input parameters are: 
    1) omega_matter -- Density of CDM, baryons, and massive neutrinos,
                in units of the critical density. 
    2) omega_baryon -- Density of baryons, in units of critical. 
    3) omega_hdm    -- Density of massive neutrinos, in units of critical 
    4) degen_hdm    -- (Int) Number of degenerate massive neutrino species 
    5) omega_lambda -- Cosmological constant 
    6) hubble       -- Hubble constant, in units of 100 km/s/Mpc 
    7) redshift     -- The redshift at which to evaluate */

/* TFmdm_onek_mpc() -- User passes a single wavenumber, in units of Mpc^-1.
    Routine returns the transfer function from the Eisenstein & Hu
    fitting formula, based on the cosmology currently held in the 
    internal variables.  The routine returns T_cb (the CDM+Baryon
    density-weighted transfer function), although T_cbn (the CDM+
    Baryon+Neutrino density-weighted transfer function) is stored
    in the global variable tf_cbnu. */

/* We also supply TFmdm_onek_hmpc(), which is identical to the previous
    routine, but takes the wavenumber in units of h Mpc^-1. */

/* We hold the internal scalar quantities in global variables, so that
the user may access them in an external program, via "extern" declarations. */

/* Please note that all internal length scales are in Mpc, not h^-1 Mpc! */

/* ------------------------- Parameters ------------------------ */

/* The following are set in TFmdm_set_cosm() */
struct transfer_function_cosmology {
    double alpha_gamma,   /* sqrt(alpha_nu) */
           alpha_nu,   /* The small-scale suppression */
           beta_c,     /* The correction to the log in the small-scale */
           num_degen_hdm,  /* Number of degenerate massive neutrino species */
           f_baryon,   /* Baryon fraction */
           f_bnu,      /* Baryon + Massive Neutrino fraction */
           f_cb,       /* Baryon + CDM fraction */
           f_cdm,      /* CDM fraction */
           f_hdm,      /* Massive Neutrino fraction */
           growth_k0,  /* D_1(z) -- the growth function as k->0 */
           growth_to_z0,   /* D_1(z)/D_1(0) -- the growth relative to z=0 */
           hhubble,    /* Need to pass Hubble constant to TFmdm_onek_hmpc() */
           k_equality, /* The comoving wave number of the horizon at equality*/
           obhh,       /* Omega_baryon * hubble^2 */
           omega_curv, /* = 1 - omega_matter - omega_lambda */
           omega_lambda_z, /* Omega_lambda at the given redshift */
           omega_matter_z, /* Omega_matter at the given redshift */
           omhh,       /* Omega_matter * hubble^2 */
           onhh,       /* Omega_hdm * hubble^2 */
           p_c,        /* The correction to the exponent before drag epoch */
           p_cb,       /* The correction to the exponent after drag epoch */
           sound_horizon_fit,  /* The sound horizon at the drag epoch */
           theta_cmb,  /* The temperature of the CMB, in units of 2.7 K */
           y_drag,     /* Ratio of z_equality to z_drag */
           z_drag,     /* Redshift of the drag epoch */
           z_equality; /* Redshift of matter-radiation equality */
};

/* The following are set in TFmdm_onek_mpc() */

/* Finally, TFmdm_onek_mpc() and TFmdm_onek_hmpc() give their answers as */
double tf_cb,     /* The transfer function for density-weighted
                   * CDM + Baryon perturbations. */
       tf_cbnu;   /* The transfer function for density-weighted
                   * CDM + Baryon + Massive Neutrino perturbations. */

/* -------------------------- Prototypes ----------------------------- */

int
TFmdm_set_cosm(struct transfer_function_cosmology *,
               double omega_matter, double omega_baryon, double omega_hdm,
               int degen_hdm, double omega_lambda, double hubble, double redshift);

/* Computes the transfer funtion with (cdm + baryons) and (cdm + baryons + hdm)
 * (tf_cb and tf_cbnu, respectively), and the growth function (growth_cb and
 * growth_cbnu, same distinction) for each wavelength k.
 *
 * Paramsters:
 *  cosm - The cosmology, initialized with `TFmdm_set_cosm`
 *  nks - The number of wavelength values to compute, the length of `kk`
 *  kk - Array of `nks` wavelengths. Units of Mpc.
 *  tf_cb - Either an array of `nks` doubles, in which the transfer function
 *          will be computed for each k, or NULL, in which case it will be
 *          ignored.
 *  tf_cbnu - Same as tf_cb, computes TF including neutrinos.
 *  growth_cb - Array to store Growth function for CDM + Baryons. Either
 *              NULL or reference to an array of `nks` doubles.
 *  growth_cbnu - Same as growth_cb, computes Growth function including neutrinos.
 */
void
TFmdm_mpc(const struct transfer_function_cosmology *cosm,
          size_t nks,
          const double *kk,
          double *tf_cb, double *tf_cbnu,
          double *growth_cb, double *growth_cbnu);

/* Same as `TFmdm_mpc`, but takes the wavelength in units of h^{-1} Mpc */
void
TFmdm_hmpc(const struct transfer_function_cosmology *cosm,
           size_t nks,
           const double *kk,
           double *tf_cb, double *tf_cbnu,
           double *growth_cb, double *growth_cbnu);

#endif
